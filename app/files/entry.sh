#!/usr/bin/env bash
#########################################################
# Function : 应用入口文件                                 #
# Platform : Linux                                      #
# Version  : 1.0.0                                      #
# Date     : 2022-05-13                                 #
# Author   : Jetsung Chan                               #
# Contact  : jetsungchan@gmail.com                      #
#########################################################

init() {
    # 应用名称
    APP_NAME="Wine-WeChatTool"

    # 应用版本
    APP_VER="1.05.2204250"

    # 应用启动文件（不可修改）
    START_SHELL_PATH="${HOME}/.wineapp/tools/run.sh"

    # 应用启动文件
    EXEC_PATH="c:/Program Files (x86)/Tencent/微信web开发者工具/微信开发者工具.exe"
    
    # 应用卸载文件
    UNINSTALL_PATH="c:/Program Files (x86)/Tencent/微信web开发者工具/卸载微信开发者工具.exe"

    # 静默安装参数
    INSTALL_QUIET="/S"

    export MIME_TYPE=""

    EXPORT_ENVS=""
    if [ -n "${EXPORT_ENVS}" ];then
        export ${EXPORT_ENVS}
    fi
}

exec() {
    RUN_PATH="${EXEC_PATH}"

    REMOVE=$(echo "$@" | grep "\-\-remove")
    [[ -z "${REMOVE}" ]] || RUN_PATH="${UNINSTALL_PATH}"

    if [ -n "${RUN_PATH}" ];then
        ${START_SHELL_PATH} ${APP_NAME} ${APP_VER} "${RUN_PATH}" "$@"
    else
        ${START_SHELL_PATH} ${APP_NAME} ${APP_VER} "uninstaller.exe" "$@"
    fi
}

main() {
    init

    [[ -z "${1}" ]] || exec "$@"
}

main "$@" || exit 1
