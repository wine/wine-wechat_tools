# Wine-WeChatTool
wine-wechat_tools for wineapp

## 安装
```bash
# 方法一（git 远程安装）
wineapp -i https://jihulab.com/wineapp/wine-wechat_tools.git

# 方法二 (中心库 远程安装)
wineapp -i com.qq.wechat_tools.wine

# 方法三 本地安装
git clone https://jihulab.com/wineapp/wine-wechat_tools.git
cd wine-demo
wineapp -i
```

## 中心库
- https://jihulab.com/wineapp/winecenter.git
```bash
# 搜索
wineapp search com.qq.wechat_tools.wine
```
